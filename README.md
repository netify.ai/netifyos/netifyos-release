# NetifyOS Release Package

## Description

NetifyOS release files and yum repository definitions.

## Version Branches

The master branch is not used in the NetifyOS project.  Instead, the version branches track the current stable development:

* netifyos7
* netifyos8
* and beyond
