%define debug_package %{nil}
%define product_family NetifyOS
%define variant_titlecase Server
%define variant_lowercase server
%define release_name Final
%define base_release_version 7
%define full_release_version 7
%define dist_release_version 7
%define upstream_rel_long 7.5-8
%define upstream_rel 7.5
%define centos_rel 5.1804
#define beta Beta
%define dist .os7
%define netifyos_rel 5.3
%define netifyos_release_version 7.5.0

Name:           netifyos-release
Version:        %{base_release_version}
Release:        %{netifyos_rel}%{?dist}
Summary:        %{product_family} release file
Group:          System Environment/Base
License:        GPLv2
URL:            https://www.netify.ai/resources/netifyos
Vendor:         Netify by eGloo
Packager:       Netify by eGloo
Provides:       netifyos-release = %{version}-%{release}
Provides:       netifyos-release(upstream) = %{upstream_rel}
Obsoletes:      redhat-release-as redhat-release-es redhat-release-ws redhat-release-de
Obsoletes:      centos-release
Obsoletes:      epel-release
Obsoletes:      clearos-release
Provides:       centos-release(upstream) = %{upstream_rel}
Provides:       redhat-release = %{upstream_rel_long}
Provides:       system-release = %{upstream_rel_long}
Provides:       system-release(releasever) = %{base_release_version}
Provides:       centos-release = %{version}
Provides:       clearos-release
Provides:       epel-release
Source:         %{name}-%{version}.tar.gz
Source1:        85-display-manager.preset
Source2:        90-default.preset
Source100: netifyos.repo
Source101: netifyos-centos.repo
Source102: netifyos-epel.repo

%description
%{product_family} release files

%prep
%setup -q -n netifyos-release-7

%build
echo OK

%install
rm -rf %{buildroot}

# create /etc
mkdir -p %{buildroot}/etc

# create /etc/system-release and /etc/redhat-release
echo "%{product_family} release %{netifyos_release_version} (%{release_name}) " > %{buildroot}/etc/netifyos-release
ln -s netifyos-release %{buildroot}/etc/system-release
ln -s netifyos-release %{buildroot}/etc/redhat-release
ln -s netifyos-release %{buildroot}/etc/centos-release
ln -s netifyos-release %{buildroot}/etc/clearos-release

# create /etc/os-release
cat << EOF >>%{buildroot}/etc/os-release
NAME="%{product_family}"
VERSION="%{full_release_version} (%{release_name})"
ID="netifyos"
ID_LIKE="rhel fedora"
VERSION_ID="%{full_release_version}"
PRETTY_NAME="%{product_family} %{full_release_version} (%{release_name})"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:netifyos:netifyos:7"
HOME_URL="https://www.netifyos.com/"
BUG_REPORT_URL="https://www.netify.ai/resources"

EOF
# write cpe to /etc/system/release-cpe
echo "cpe:/o:netifyos:netifyos:7" > %{buildroot}/etc/system-release-cpe

# create /etc/issue and /etc/issue.net
echo '\S' > %{buildroot}/etc/issue
echo 'Kernel \r on an \m' >> %{buildroot}/etc/issue
cp %{buildroot}/etc/issue %{buildroot}/etc/issue.net
echo >> %{buildroot}/etc/issue

# copy GPG keys
mkdir -p -m 755 %{buildroot}/etc/pki/rpm-gpg
for file in RPM-GPG-KEY* ; do
    install -m 644 $file %{buildroot}/etc/pki/rpm-gpg
done

# copy yum repos
mkdir -p -m 755 %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE100} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE101} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE102} %{buildroot}/etc/yum.repos.d

# set up the dist tag macros
install -d -m 755 %{buildroot}/etc/rpm
cat >> %{buildroot}/etc/rpm/macros.dist << EOF
# dist macros.

%%netifyos_ver %{base_release_version}
%%netifyos %{base_release_version}
%%centos_ver %{base_release_version}
%%centos %{base_release_version}
%%rhel %{base_release_version}
%%dist %dist
%%el%{base_release_version} 1
EOF

# use unbranded datadir
mkdir -p -m 755 %{buildroot}/%{_datadir}/netifyos-release
ln -s netifyos-release %{buildroot}/%{_datadir}/redhat-release
install -m 644 EULA %{buildroot}/%{_datadir}/netifyos-release

# use unbranded docdir
mkdir -p -m 755 %{buildroot}/%{_docdir}/netifyos-release
ln -s netifyos-release %{buildroot}/%{_docdir}/redhat-release
install -m 644 GPL %{buildroot}/%{_docdir}/netifyos-release

# copy systemd presets
mkdir -p %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -m 0644 %{SOURCE1} %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -m 0644 %{SOURCE2} %{buildroot}%{_prefix}/lib/systemd/system-preset/


%clean
rm -rf %{buildroot}

%files
%defattr(0644,root,root,0755)
/etc/redhat-release
/etc/system-release
/etc/centos-release
/etc/clearos-release
/etc/netifyos-release
%config(noreplace) /etc/os-release
%config /etc/system-release-cpe
%config(noreplace) /etc/issue
%config(noreplace) /etc/issue.net
/etc/pki/rpm-gpg/
%config(noreplace) /etc/yum.repos.d/*
/etc/rpm/macros.dist
%{_docdir}/redhat-release
%{_docdir}/netifyos-release/*
%{_datadir}/redhat-release
%{_datadir}/netifyos-release/*
%{_prefix}/lib/systemd/system-preset/*
